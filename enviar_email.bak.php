<?php

if($_POST){

    // Variáveis Geral
    $nomeCliente = $_POST['nome'];
    $emailCliente = $_POST['email'];
    $emailEmpresa = "contato@milblocos.pe.hu";
    $assuntoE1 = '=?UTF-8?B?'.base64_encode("Mil Blocos - Formulário do Website").'?=';
    $assuntoE2 = '=?UTF-8?B?'.base64_encode($_POST['assunto']).'?=';
    $mensagem = $_POST['mensagem'];

    // Variáveis e-mail cliente
    $headers   = array();
    $headers[] = "Content-type: text/html;";
    $headers[] = "From: Mil Blocos <$emailEmpresa>";
    $headers[] = "Subject: {$assuntoE1}";
    $headers[] = "X-Mailer: PHP/".phpversion();

    // Envia variáveis para o template e substitui
    $variaveis['nomeCliente'] = $nomeCliente;
    $variaveis['mensagem'] = $mensagem;
    $variaveis['anoAtual'] = date('Y');

    $template = file_get_contents("email_templates/default.html");

    foreach($variaveis as $key => $var)
    {
        $template = str_replace('{{ '.$key.' }}', $var, $template);
    }

    // Enviar e-mail para o cliente
    if(mail($emailCliente, $assuntoE1, $template, implode("\r\n", $headers))){

      // Variáveis e-mail empresa
      $headers   = array();
      $headers[] = "Content-type: text/html;";
      $headers[] = "From: $nomeCliente <$emailCliente>";
      $headers[] = "Subject: {$assuntoE2}";
      $headers[] = "X-Mailer: PHP/".phpversion();
      // Enviar e-mail para a empresa
      if(mail($emailEmpresa, $assuntoE2, $mensagem, implode("\r\n", $headers))){
        echo json_encode(array('enviou' => 's'));
      }else{
        echo json_encode(array('enviou' => 'n'));
      }
    }else{
      echo json_encode(array('enviou' => 'n'));
    }
}else{
  echo json_encode(array('enviou' => 'n'));
}die;
?>
