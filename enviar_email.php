<?php

if($_POST){

    require 'plugins/PHPMailer/PHPMailerAutoload.php';
    // Variáveis Gerais
    $nomeCliente = $_POST['nome'];
    $emailCliente = $_POST['email'];
    $emailEmpresa = "contato@milblocos.pe.hu";
    $assuntoE1 = '=?UTF-8?B?'.base64_encode("Mil Blocos - Formulário do Website").'?=';
    $assuntoE2 = '=?UTF-8?B?'.base64_encode($_POST['assunto']).'?=';
    $mensagem = $_POST['mensagem'];

    // Variáveis PHPMailer Cliente
    $mail = new PHPMailer();
    $mail->IsMail();
    $mail->IsHTML(true);
    $mail->CharSet = 'UTF-8';
    // $mail->Host = 'smtp.gmail.com';
    // $mail->SMTPAuth = true;
    // $mail->Username = 'formulario@milblocos.pe.hu';
    // $mail->Password = 'formulario@formulario';
    // $mail->SMTPSecure = 'tls';
    // $mail->Port = 587;

    // // Envia variáveis para o template e substitui
    $variaveis['nomeCliente'] = $nomeCliente;
    $variaveis['mensagem'] = $mensagem;
    $variaveis['anoAtual'] = date('Y');

    $template = file_get_contents("email_templates/default.html");

    foreach($variaveis as $key => $var)
    {
        $template = str_replace('{{ '.$key.' }}', $var, $template);
    }

    // $mail->From = $emailEmpresa;
    // $mail->FromName = 'Mil Blocos';
    $mail->setFrom($emailEmpresa, 'Mil Blocos');
    $mail->addAddress($emailCliente);
    // $mail->isHTML(true);
    $mail->Subject = $assuntoE1;
    $mail->MsgHTML($template);

    // Enviar e-mail para o cliente
    if($mail->send()){

      // Variáveis PHPMailer Empresa
      $mail = new PHPMailer();
      $mail->IsMail();
      $mail->IsHTML(true);
      $mail->CharSet = 'UTF-8';
      // $mail->Host = 'smtp.gmail.com';
      // $mail->SMTPAuth = true;
      // $mail->Username = 'formulario@milblocos.pe.hu';
      // $mail->Password = 'formulario@formulario';
      // $mail->Port = 587;
      // $mail->SMTPSecure = 'tls';
      $mail->setFrom($emailCliente, $nomeCliente);
      $mail->isHTML(true);
      $mail->addAddress($emailEmpresa);
      $mail->Subject = $assuntoE2;
      $mensagem = $mensagem;
      $mail->MsgHTML($mensagem);
      // // Enviar e-mail para a empresa
      if($mail->send()){
        echo json_encode(array('enviou' => 's'));
      }else{
        echo json_encode(array('enviou' => 'n'));
      }
    }else{
      echo json_encode(array('enviou' => 'n'));
    }
}else{
  echo json_encode(array('enviou' => 'n'));
}die;
