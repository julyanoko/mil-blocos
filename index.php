<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if gt IE 9]> <html lang="en" class="ie"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->

<head>
		<meta charset="utf-8">
		<title>Mil Blocos - Solução em Armazenagem</title>
		<meta name="description" content="Mil Blocos - Solução em Armazenagem">
		<meta name="author" content="Julyano">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="imagens/icone.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

		<!-- Bootstrap core CSS -->
		<link href="bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="plugins/rs-plugin/css/settings.css" rel="stylesheet">
		<link href="css/animations.css" rel="stylesheet">

		<!-- The Project's core CSS file -->
		<link href="css/style.css" rel="stylesheet" >

	</head>

	<body class="no-trans page-loader-2 front-page transparent-header">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>

		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">

			<!-- header-container start -->
			<div class="header-container">
				<!-- header start -->
				<div class="header-top dark ">
					<div class="container">
						<div class="row">
							<div class="col-xs-3 col-sm-6 col-md-9">
								<!-- header-top-first start -->
								<!-- ================ -->
								<div class="header-top-first clearfix">
									<ul class="social-links circle small clearfix hidden-xs">
										<li class="twitter"><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>
										<li class="skype"><a target="_blank" href="#"><i class="fa fa-skype"></i></a></li>
										<li class="linkedin"><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></li>
										<li class="googleplus"><a target="_blank" href="#"><i class="fa fa-google-plus"></i></a></li>
										<li class="youtube"><a target="_blank" href="#"><i class="fa fa-youtube-play"></i></a></li>
										<li class="flickr"><a target="_blank" href="#"><i class="fa fa-flickr"></i></a></li>
										<li class="facebook"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a></li>
										<li class="pinterest"><a target="_blank" href="#"><i class="fa fa-pinterest"></i></a></li>
									</ul>
									<div class="social-links hidden-lg hidden-md hidden-sm circle small">
										<div class="btn-group dropdown">
											<button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-share-alt"></i></button>
											<ul class="dropdown-menu dropdown-animation">
												<li class="twitter"><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>
												<li class="skype"><a target="_blank" href="#"><i class="fa fa-skype"></i></a></li>
												<li class="linkedin"><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></li>
												<li class="googleplus"><a target="_blank" href="#"><i class="fa fa-google-plus"></i></a></li>
												<li class="youtube"><a target="_blank" href="#"><i class="fa fa-youtube-play"></i></a></li>
												<li class="flickr"><a target="_blank" href="#"><i class="fa fa-flickr"></i></a></li>
												<li class="facebook"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a></li>
												<li class="pinterest"><a target="_blank" href="#"><i class="fa fa-pinterest"></i></a></li>
											</ul>
										</div>
									</div>
									<ul class="list-inline hidden-sm hidden-xs">
										<li><i class="fa fa-map-marker pr-5 pl-10"></i>Rua Cyro Correia Pereira, 667 - Curitiba</li>
										<li><i class="fa fa-phone pr-5 pl-10"></i>(41) 99721-0588</li>
										<li><i class="fa fa-envelope-o pr-5 pl-10"></i><a href="mailto:contato@milblocos.pe.hu">contato@milblocos.pe.hu</a></li>
									</ul>
								</div>
								<!-- header-top-first end -->
							</div>
						</div>
					</div>
				</div>

				<header class="header  fixed    clearfix">

					<div class="container">
						<div class="row">
							<div class="col-md-9">

								<!-- header-right start -->
								<!-- ================ -->
								<div class="header-right clearfix">

								<!-- main-navigation start -->
								<div class="main-navigation  animated with-dropdown-buttons">

									<!-- navbar start -->
									<!-- ================ -->
									<nav class="navbar navbar-default" role="navigation">
										<div class="container-fluid">

											<!-- Toggle get grouped for better mobile display -->
											<div class="navbar-header">
												<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
													<span class="sr-only">Toggle navigation</span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
												</button>

											</div>

											<!-- Collect the nav links, forms, and other content for toggling -->
											<div class="collapse navbar-collapse" id="navbar-collapse-1">
												<!-- main-menu -->
												<ul class="nav navbar-nav ">

													<!-- mega-menu start -->
													<li class="">
														<a href="#mn1" class="btn btn-lg moving smooth-scroll">Início</a>
													</li>
													<li class="">
														<a href="#mn2" class="btn btn-lg moving smooth-scroll">Nossos Serviços</a>
													</li>
													<li class="dropdown">
														<a class="btn btn-lg moving smooth-scroll" data-toggle="dropdown" href="#mn3">Preços</a>
														<ul class="dropdown-menu">
															<li><a href="#mn3" class="btn btn-lg moving smooth-scroll">Tabela de preços - Box</a></li>
															<li><a href="#mn4" class="btn btn-lg moving smooth-scroll">Tabela de preços - Palete</a></li>
															<li><a href="#mn5" class="btn btn-lg moving smooth-scroll">Tabela de preços - Outros Serviços</a></li>
														</ul>
													</li>
													<li class="">
														<a href="#footer" class="btn btn-lg moving smooth-scroll">Contato</a>
													</li>
													<!-- mega-menu end -->

											</div>

										</div>
									</nav>
									<!-- navbar end -->

								</div>
								<!-- main-navigation end -->
								</div>
								<!-- header-right end -->

							</div>
						</div>
					</div>

				</header>
				<!-- header end -->
			</div>
			<!-- header-container end -->
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix" id="mn1">

				<!-- slideshow start -->
				<!-- ================ -->
				<div class="slideshow">

					<!-- slider revolution start -->
					<!-- ================ -->
					<div class="slider-banner-container parallax">
						<div class="slider-banner-fullscreen">
							<ul class="slides">
								<!-- slide 1 start -->
								<!-- ================ -->
								<li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Principal">

								<!-- main image -->
								<img src="imagens/header-1.jpg" alt="slidebg1" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">

								<!-- Transparent Background -->
								<div class="tp-caption dark-translucent-bg"
									data-x="center"
									data-y="center"
									data-start="0"
									data-transform_idle="o:1;"
									data-transform_in="o:0;s:600;e:Power2.easeInOut;"
									data-transform_out="o:0;s:600;"
									data-width="5000"
									data-height="5000">
								</div>

								<!-- LAYER NR. 1 -->
								<div class="tp-caption sfr stl xlarge_white"
									data-x="center"
									data-y="70"
									data-speed="200"
									data-easing="easeOutQuad"
									data-start="1000"
									data-end="2500"
									data-splitin="chars"
									data-elementdelay="0.1"
									data-endelementdelay="0.1"
									data-splitout="chars">Segurança
								</div>

								<!-- LAYER NR. 2 -->
								<div class="tp-caption sfl str xlarge_white"
									data-x="center"
									data-y="70"
									data-speed="200"
									data-easing="easeOutQuad"
									data-start="2500"
									data-end="4000"
									data-splitin="chars"
									data-elementdelay="0.1"
									data-endelementdelay="0.1"
									data-splitout="chars">Inovação
								</div>

								<!-- LAYER NR. 3 -->
								<div class="tp-caption sfr stt xlarge_white"
									data-x="center"
									data-y="70"
									data-speed="200"
									data-easing="easeOutQuad"
									data-start="4000"
									data-end="6000"
									data-splitin="chars"
									data-elementdelay="0.1"
									data-endelementdelay="0.1"
									data-splitout="chars"
									data-endspeed="400">Localização
								</div>

								<!-- LAYER NR. 5 -->
								<div class="tp-caption sfr fadeout"
									data-x="center"
									data-y="210"
									data-hoffset="-232"
									data-speed="500"
									data-easing="easeOutQuad"
									data-start="1000"
									data-end="5500"><span class="icon large circle"><i class="icon-lock-filled"></i></span>
								</div>

								<!-- LAYER NR. 6 -->
								<div class="tp-caption sfl fadeout"
									data-x="center"
									data-y="210"
									data-speed="500"
									data-easing="easeOutQuad"
									data-start="2500"
									data-end="5500"><span class="icon large circle"><i class="icon-lightbulb"></i></span>
								</div>

								<!-- LAYER NR. 7 -->
								<div class="tp-caption sfr fadeout"
									data-x="center"
									data-y="210"
									data-hoffset="232"
									data-speed="500"
									data-easing="easeOutQuad"
									data-start="4000"
									data-end="5500"><span class="icon large circle"><i class="icon-location-1"></i></span>
								</div>

								<!-- LAYER NR. 10 -->
								<div class="tp-caption fade fadeout"
									data-x="center"
									data-y="bottom"
									data-voffset="100"
									data-speed="500"
									data-easing="easeOutQuad"
									data-start="1000"
									data-end="5500"
									data-endspeed="200">
									<a href="#page-start" class="btn btn-lg moving smooth-scroll"><i class="icon-down-open-big"></i><i class="icon-down-open-big"></i><i class="icon-down-open-big"></i></a>
								</div>
								</li>
								<!-- slide 1 end -->

								<!-- slide 2 start -->
								<!-- ================ -->
								<li data-transition="random" data-slotamount="7" data-masterspeed="default" data-title="Sobre">

								<!-- main image -->
								<img src="imagens/header-2.jpg" alt="slidebg2" data-bgposition="center center" data-bgrepeat="no-repeat" data-bgfit="cover" class="rev-slidebg">

								<!-- Transparent Background -->
								<div class="tp-caption dark-translucent-bg"
									data-x="center"
									data-y="center"
									data-start="0"
									data-transform_idle="o:1;"
									data-transform_in="o:0;s:600;e:Power2.easeInOut;"
									data-transform_out="o:0;s:600;"
									data-width="5000"
									data-height="5000">
								</div>

								<!-- LAYER NR. 1 -->
								<div class="tp-caption large_white text-right"
									data-x="right"
									data-y="200"
									data-start="500"
									data-transform_idle="o:1;"
									data-transform_in="y:[100%];sX:1;sY:1;o:0;s:2000;e:Power4.easeInOut;"
									data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
									data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
									data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">
										<span class="logo-font">Mil Blocos</span>
								</div>

								<!-- LAYER NR. 2 -->
								<div class="tp-caption large_white tp-resizeme"
									data-x="right"
									data-y="270"
									data-start="750"
									data-transform_idle="o:1;"
									data-transform_in="o:0;s:2000;e:Power4.easeInOut;">
										<div class="separator-3 light"></div>
								</div>

								<!-- LAYER NR. 3 -->
								<div class="tp-caption medium_white text-right"
									data-x="right"
									data-y="290"
									data-start="750"
									data-transform_idle="o:1;"
									data-transform_in="y:[100%];sX:1;sY:1;s:1750;e:Power4.easeInOut;"
									data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
									data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
									data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">A Mil Blocos é de fácil acesso ao centro da cidade de Curitiba,<br /> conta com uma infraestrutura de boxes moduláveis e posições <br />paletes que você precisa.
								</div>

								<!-- LAYER NR. 4 -->
								<div class="tp-caption fade fadeout"
									data-x="center"
									data-y="bottom"
									data-voffset="100"
									data-speed="500"
									data-easing="easeOutQuad"
									data-start="500"
									data-endspeed="200">
									<a href="#page-start" class="btn btn-lg moving smooth-scroll"><i class="icon-down-open-big"></i><i class="icon-down-open-big"></i><i class="icon-down-open-big"></i></a>
								</div>
								</li>
								<!-- slide 2 end -->

								<!-- slide 3 start -->
								<!-- ================ -->
								<!-- <li data-transition="random" data-slotamount="7" data-masterspeed="default" data-title="Natal">

								<img src="imagens/header-promocao.png" alt="slidebg2" data-bgposition="center center" data-bgrepeat="no-repeat" data-bgfit="cover" class="rev-slidebg">

								<div class="tp-caption dark-translucent-bg"
									data-x="center"
									data-y="center"
									data-start="0"
									data-transform_idle="o:1;"
									data-transform_in="o:0;s:600;e:Power2.easeInOut;"
									data-transform_out="o:0;s:600;"
									data-width="5000"
									data-height="5000">
								</div>

								<div class="tp-caption sfb fadeout large_white"
									data-x="center"
									data-y="200"
									data-start="500"
									data-easing="easeOutQuad"
									data-end="10000">Mês de Dezembro
								</div>

								<div class="tp-caption large_white tp-resizeme"
									data-x="center"
									data-y="270"
									data-start="750"
									data-easing="easeOutQuad">
										<div class="separator-3 light"></div>
								</div>

								<div class="tp-caption medium_white text-right"
									data-x="center"
									data-y="290"
									data-start="750"
									data-easing="easeOutQuad">Promoção de 10% para todos os pacotes Mini.
								</div>

								<div class="tp-caption fade fadeout"
									data-x="center"
									data-y="bottom"
									data-voffset="100"
									data-speed="500"
									data-easing="easeOutQuad"
									data-start="500"
									data-endspeed="200">
									<a href="#page-start" class="btn btn-lg moving smooth-scroll"><i class="icon-down-open-big"></i><i class="icon-down-open-big"></i><i class="icon-down-open-big"></i></a>
								</div>
								</li> -->
								<!-- slide 3 end -->
							</ul>
							<div class="tp-bannertimer"></div>
						</div>
					</div>
					<!-- slider revolution end -->

				</div>
				<!-- slideshow end -->

			</div>
			<!-- banner end -->

			<div id="page-start"></div>


			<!-- section start -->
			<!-- ================ -->
			<section class="pv-40" id="mn2">
				<div class="container">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-12">
							<!-- page-title start -->
							<!-- ================ -->
							<h1 class="page-title text-center">Nossos <strong>Serviços</strong></h1>
							<div class="separator"></div>
							<br>
							<!-- page-title end -->

							<div class="row">
								<div class="col-sm-6">
									<div class="image-box style-2 mb-20">
										<div class="overlay-container overlay-visible">
											<img src="imagens/armazem-boxes.jpg" alt="" style>
											<div class="overlay-bottom">
												<div class="text">
													<p class="lead margin-clear mobile-visible">Aluguel de box</p>
												</div>
											</div>
										</div>
										<div class="body text-center padding-horizontal-clear">
											<p>Possui locação mínima de 1m³ e período mínimo de contratação de 20 dias. São 100% individuais e possuem o sistema de segurança por biometria. Todos os bens redigidos no contrato são assegurados pela Porto Seguro e seu monitoramento é realizado 24 horas por dia.</p>
										</div>
										*OBS: Não é permitido armazenagem de materiais ou produtos ilícitos e/ou vivos.
									</div>
								</div>
								<div class="col-sm-6">
									<div class="image-box style-2 mb-20">
										<div class="overlay-container overlay-visible">
											<img src="imagens/armazenagem-pallet.jpg" alt="">
											<div class="overlay-bottom">
												<div class="text">
													<p class="lead margin-clear mobile-visible">Aluguel de posição palete</p>
												</div>
											</div>
										</div>
										<div class="body text-center padding-horizontal-clear">
											<p>Possui locação mínima de uma posição palete e período mínimo de contratação é de 20 dias. O espaço alugado no porta palete é individual e possui controle de gestão de acessos. Todos os bens redigidos no contrato são assegurados pela Porto Seguro e seu monitoramento é realizado 24 horas por dia.</p>
										</div>
										*OBS: Não é permitido armazenagem de materiais ou produtos ilícitos e/ou vivos.
									</div>
								</div>
							</div>
						</div>
						<!-- main end -->

					</div>
				</div>
			</section>
			<!-- section end -->


			<!-- main-container start -->
			<!-- ================ -->
			<section class="main-container">

				<div class="container">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-12">

							<!-- page-title start -->
							<!-- ================ -->
							<hr id="mn3" /><br />
							<h1 class="page-title text-center">Tabela de <strong>Preços</strong> - Box</h1>
							<div class="separator"></div>
							<br>
							<!-- page-title end -->

							<!-- pricing tables start -->
							<!-- ================ -->
							<div class="pricing-tables stripped object-non-visible" data-animation-effect="fadeInUpSmall"  data-effect-delay="0">
								<div class="row grid-space-0">
									<!-- pricing table start -->
									<!-- ================ -->
									<div class="col-sm-4 plan stripped">
										<div class="header dark-bg">
											<h3><a href="#" class="pt-popover" data-toggle="popover" data-placement="bottom" data-content="Aluguel de box de 1m³." title="">Box Mini</a></h3>
											<div class="price"><span>R$120,00</span>/mês</div>
										</div>
										<ul>
											<li>Armazenagem</li>
											<li>Monitoramento</li>
											<li><i class="fa fa-close" style="color:red"></i></li>
											<li><i class="fa fa-close" style="color:red"></i></li>
											<li><i class="fa fa-close" style="color:red"></i></li>
										</ul>
									</div>
									<!-- pricing table end -->

									<!-- pricing table start -->
									<!-- ================ -->
									<div class="col-sm-4 plan stripped best-value">
										<div class="header default-bg">
											<h3><a href="#" class="pt-popover" data-toggle="popover" data-placement="bottom" data-content="Aluguel de box a partir de 7m³." title="">Combo Mix</a></h3>
											<div class="price"><span>A partir de R$915,00</span>/mês</div>
										</div>
										<ul>
											<li>Armazenagem</li>
											<li>Monitoramento</li>
											<li>Manuseio</li>
											<li>Embalagem</li>
											<li>Transporte</li>
										</ul>
									</div>
									<!-- pricing table end -->

									<!-- pricing table start -->
									<!-- ================ -->
									<div class="col-sm-4 plan stripped">
										<div class="header dark-bg">
											<h3><a href="#" class="pt-popover" data-toggle="popover" data-placement="bottom" data-content="Aluguel de box de 1m³ à 7m³." title="">Box Plus</a></h3>
											<div class="price"><span>A partir de R$415,00</span>/mês</div>
										</div>
										<ul>
											<li>Armazenagem</li>
											<li>Monitoramento</li>
											<li><i class="fa fa-close" style="color:red"></i></li>
											<li><i class="fa fa-close" style="color:red"></i></li>
											<li><i class="fa fa-close" style="color:red"></i></li>
										</ul>
									</div>
									<!-- pricing table end -->
								</div>
							</div>
							<!-- pricing tables end -->

							<hr id="mn4" /><br />
							<h1 class="page-title text-center">Tabela de <strong>Preços</strong> - Palete</h1>
							<div class="separator"></div>
							<br>
							<!-- pricing tables start -->
							<!-- ================ -->
							<div class="pricing-tables stripped object-non-visible" data-animation-effect="fadeInUpSmall"  data-effect-delay="0">
								<div class="row grid-space-0">
									<!-- pricing table start -->
									<!-- ================ -->
									<div class="col-sm-6 plan stripped">
										<div class="header dark-bg">
											<h3><a href="#" class="pt-popover" data-toggle="popover" data-placement="bottom" data-content="Aluguel de 1 posição palete." title="">Palete Mini</a></h3>
											<div class="price"><span>A partir de R$98,00</span>/mês</div>
										</div>
										<ul>
											<li>Armazenagem</li>
											<li>Monitoramento</li>
											<li>Manuseio</li>
											<li>Embalagem</li>
											<li><i class="fa fa-close" style="color:red"></i></li>
										</ul>
									</div>
									<!-- pricing table end -->

									<!-- pricing table start -->
									<!-- ================ -->
									<div class="col-sm-6 plan stripped best-value">
										<div class="header default-bg">
											<h3><a href="#" class="pt-popover" data-toggle="popover" data-placement="bottom" data-content="Aluguel de mais de 5 posições paletes." title="">Palete Plus</a></h3>
											<div class="price"><span>A partir de R$840,00</span>/mês</div>
										</div>
										<ul>
											<li>Armazenagem</li>
											<li>Monitoramento</li>
											<li>Manuseio</li>
											<li>Embalagem</li>
											<li>Transporte</li>
										</ul>
									</div>
									<!-- pricing table end -->

								</div>
							</div>
							<!-- pricing tables end -->

							<hr id="mn5" /><br />
							<h1 class="page-title text-center">Tabela de <strong>Preços</strong> - Outros Serviços</h1>
							<div class="separator"></div>
							<br>

							<!-- pricing tables start -->
							<!-- ================ -->
							<div class="pricing-tables stripped object-non-visible" data-animation-effect="fadeInUpSmall"  data-effect-delay="0">
								<div class="row grid-space-0">
									<!-- pricing tables end -->
									<div class="col-sm-6 plan stripped">
										<div class="header dark-bg">
											<h3><a href="#" class="pt-popover" data-toggle="popover" data-placement="bottom" data-content="Distância mínima de 5 km." title="">Transporte</a></h3>
											<div class="price"><span>A partir de R$12,00</span>/km</div>
										</div>
										<ul>
											<li>Taxa de Cobrança por km rodado</li>
										</ul>
									</div>
									<!-- pricing table end -->

									<!-- pricing table start -->
									<!-- ================ -->
									<div class="col-sm-6 plan stripped">
										<div class="header dark-bg">
											<h3><a href="#" class="pt-popover" data-toggle="popover" data-placement="bottom" data-content="Venda realizada por pacotes de três embalagens." title="">Embalagem</a></h3>
											<div class="price"><span>A partir de R$12,00</span>/pacote</div>
										</div>
										<ul>
											<li>Embalagens (papelão) sob medida e a pronta entrega</li>
											<li>Stretch (manuseio) incluso no pacote</li>
											<li>Paletes (ativo comodatado)</li>
										</ul>
									</div>
									<!-- pricing table end -->

								</div>
							</div>
						</div>
						<!-- main end -->

					</div>
				</div>
			</section>
			<!-- main-container end -->
			<!-- ================ -->
			<footer id="footer" class="clearfix ">

				<!-- .footer start -->
				<!-- ================ -->
				<div class="footer">
					<div class="container">
						<div class="footer-inner">
							<div class="row">
								<div class="col-md-8" style="text-align:center">
									<div class="footer-content">
										<div class="col-md10">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3600.1172243409915!2d-49.31635898498371!3d-25.53447188374037!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dcfce53321c61b%3A0x792ee8cefe6b8dd7!2sRua+Cyro+Correia+Pereira%2C+667+-+Cidade+Industrial%2C+Curitiba+-+PR%2C+81170-230!5e0!3m2!1spt-BR!2sbr!4v1476482931042" frameborder="0" style="border:0;height:350px;width:600px;" allowfullscreen></iframe>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="footer-content">
										<h2 class="title">Contato</h2>
										<br>
										<div class="alert alert-success hidden" id="MessageSent2">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											Nós recebemos seu e-mail e estaremos entranto em contato em breve.
										</div>
										<div class="alert alert-danger hidden" id="MessageNotSent2">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											Oops! Algo deu errado ao enviar o e-mail. Tente novamente mais tarde.
										</div>
										<form role="form" id="footer-form" class="margin-clear">
											<div class="form-group has-feedback">
												<label class="sr-only" for="name2">Nome</label>
												<input type="text" class="form-control" id="nome" placeholder="Nome" name="nome">
												<i class="fa fa-user form-control-feedback"></i>
											</div>
											<div class="form-group has-feedback">
												<label class="sr-only" for="email2">E-mail</label>
												<input type="email" class="form-control" id="email" placeholder="E-mail" name="email">
												<i class="fa fa-envelope form-control-feedback"></i>
											</div>
											<div class="form-group has-feedback">
												<label class="sr-only" for="message2">Mensagem</label>
												<textarea class="form-control" rows="6" id="mensagem" placeholder="Mensagem" name="mensagem"></textarea>
												<i class="fa fa-pencil form-control-feedback"></i>
											</div>
											<input type="submit" value="Enviar" class="margin-clear submit-button btn btn-success">
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .footer end -->

				<!-- .subfooter start -->
				<!-- ================ -->
				<div class="subfooter">
					<div class="container">
						<div class="subfooter-inner">
							<div class="row">
								<div class="col-md-12">
									<p class="text-center">
										Copyright <?= date('Y') ?> <a href="#mn1" class="btn btn-lg moving smooth-scroll" style="color: #3697d9 !important; font-size: 14px !important; line-height: 18px !important; font-weight: normal !important; font-family: helvetica, Arial, sans-serif !important; text-decoration:none !important; padding:0 !important; margin: 0 !important">Mil Blocos</a>. Todos os direitos reservados. <br />
										Desenvolvido por <a href="http://julyano.pe.hu" target="_blank" style="color: #3697d9; font-size: 14px; line-height: 18px; font-weight: normal; font-family: helvetica, Arial, sans-serif; text-decoration:none;">Julyano</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .subfooter end -->

			</footer>
			<!-- footer end -->

		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="plugins/jquery.min.js"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
		<!-- Modernizr javascript -->
		<script type="text/javascript" src="plugins/modernizr.js"></script>
		<!-- jQuery Revolution Slider  -->
		<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<!-- Appear javascript -->
		<script type="text/javascript" src="plugins/waypoints/jquery.waypoints.min.js"></script>
		<!-- Contact form -->
		<script src="plugins/jquery.validate.js"></script>
		<!-- Pace javascript -->
		<script type="text/javascript" src="plugins/pace/pace.min.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="plugins/SmoothScroll.js"></script>
		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="js/template.js"></script>

	</body>

</html>
